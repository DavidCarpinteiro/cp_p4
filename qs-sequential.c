#include <stdio.h>
#include <stdlib.h>

#include <time.h>
#include <sys/time.h>
#include <assert.h>
#include <zconf.h>

#ifdef QSPARALLEL
#include "qs-parallel.h"
#define qsort qsort_parallel
#define VERSION "Parallel"
#else
#define VERSION "Sequential"
#endif

#define TYPE double

//for i in {1..10}; do var=$(./qs-sequential 1000000 | grep -o -E '[0-9]+'); echo $var; done
//for i in {1..10}; do var=$(./qs-sequential 1000000 | grep -o -E '[0-9]+' | sed -n '2 p'); echo $var; done

    //gperf

    //man bash

int size_diff;

long long wall_clock_time(void) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (long long int) (tv.tv_sec * 1e6 + tv.tv_usec);
}

int less_than(const void* a, const void* b) {
    TYPE v1 = *(TYPE *)a;
    TYPE v2 = *(TYPE *)b;

    if (v1 < v2)
        return -1;
    if (v1 > v2)
        return 1;
    return 0;
}

int main(int argc, char* argv[]) {
    int i, N;
    long long start, end;

    if (argc != 3) {
        printf("Usage: ./quicksort-seq N S\n");
        return -1;
    }

    srand48(time(NULL));
    N = (int) atol(argv[1]);
    size_diff = (int) atol(argv[2]);

    TYPE* array = malloc(sizeof(*array) * N);
    for (i = 0; i < N; i++)
        array[i] = drand48();

    start = wall_clock_time();
    qsort(array, (size_t) N, sizeof(*array), less_than);
    end = wall_clock_time();

    for(int k = 1; k < N; k++) {
        assert(array[k-1] < array[k]);
    }

    printf("QuickSort %s: Array size = %d, Wall clock elapsed time = %ld ms\n", VERSION, N, (long) (end-start) / 1000);

    free(array);

    return 0;
}
