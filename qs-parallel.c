#include "qs-parallel.h"
#include <cilk/cilk.h>
#include <stdio.h>
#include <memory.h>
#include <assert.h>

extern int size_diff;

void swap(void *a, void *b, size_t width) {
    char tmp[width];

    memcpy(tmp, a, width);
    memcpy(a, b, width);
    memcpy(b, tmp, width);
}

//valgrind --tool=memcheck ./qs-parallel 1000

//sudo perf record ./qs-parallel 1000000
//sudo perf report

//TODO optimize with bubble sort
void bubbleSort(void *arr, int start, int end, size_t width, int (*compar)(const void *, const void *)) {
    for (int c = start; c < end; c++) {
        for (int d = start; d < end - c; d++) {
            if (compar(arr + d * width, arr + (d + 1) * width) == 1)
                swap(arr + d * width, arr + ((d + 1) * width), width);
        }
    }
}

int partition(void *arr, int start, int end, size_t nel, size_t width, int (*compar)(const void *, const void *)) {
    //char *barr = (char *)arr;
    void* pivot = arr+end*width;
    int i = (start - 1);  // Index of smaller element

    for (int j = start; j < end ; j++) {
        // If current element is smaller than or equal to pivot

        //assert(j >= 0 && j < nel);
        if (compar(arr+j*width, pivot) == -1) {
            // increment index of smaller element
            i++;
            //assert(i >= 0 && i < nel);
            swap(arr+i*width, arr+j*width, width);
        }
    }

    //assert(i + 1 >= 0 && end >= 0 && i + 1 < nel && end < nel);
    swap(arr+(i + 1)*width, arr+end*width, width);
    return (i + 1);
}

void quickSort(void *arr, int start, int end, size_t nel, size_t width, int (*compar)(const void *, const void *)) {
    if(end-start <= size_diff) {
        //BubbleSort
        
    }

    if (start < end) {
        //pi is partitioning index, arr[pi] is now at right place
        int pi = partition(arr, start, end, nel, width, compar);

        // Separately sort elements before partition and after partition
        cilk_spawn quickSort(arr, start, pi - 1, nel, width, compar);
        quickSort(arr, pi + 1, end, nel, width, compar);
    }
}

void qsort_parallel(void *base, size_t nel, size_t width, int (*compar)(const void *, const void *)) {
    quickSort(base, 0, (int) nel - 1, nel, width, compar);
}

/*
void swap2(void *a, void *b, size_t width) {
    //printf("%lf, %lf\n", *(double *)a, *(double *)b);
    double tmp;
    tmp=*(double*)a;
    *(double*)a = *(double*)b;
    *(double*)b = tmp;
}
*/